module.exports = {
  configureWebpack: {
    devtool: 'source-map',
      resolve: {
        alias: {
          "@": `${__dirname}/src`,
        },
      },
      externals: {
        AMap: "AMap",
      },
    },
    chainWebpack: config => {
      config.plugin('html').tap(args => {
        args[0].title = '配网设备地图系统'
        return args
      })
    },
    devServer: {
      port: 3000, // 端口
    },
};
