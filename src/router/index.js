import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home";
import Login from "@/views/Login";
import UserManage from "@/views/UserManage";
import Map from "@/views/Map";
import Org from "@/views/Org";
import DevicesManage from "@/views/DevicesManage";
import DevicesName from "@/views/DevicesName";

import Substation from "@/views/Substation";
import Lines from "@/views/Lines";
import RunLines from '@/views/RunLines';
import UdcManage from '@/views/UdcManage';

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/",
    redirect: "/login", //重定向到登录页面
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
    children: [
      {
        path: "Substation",
        name: "Substation",
        component: Substation,
      },
      {
        path: "devices",
        name: "devices",
        component: DevicesManage,
      },
      {
        path: "devicesName",
        name: "devicesName",
        component: DevicesName,
      },
      {
        path: "lines",
        name: "lines",
        component: Lines,
      },
      {
        path: "runLines",
        name: "runLines",
        component: RunLines,
      },
      {
        path: "user",
        name: "user",
        component: UserManage,
      },
      {
        path: "org",
        name: "org",
        component: Org,
      },
      {
        path: "map",
        name: "map",
        component: Map,
      },
      {
        path:"udc",
        name:"udc",
        component:UdcManage
      }
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
// {
//   path: "/home",
//   name: "About",
//   // route level code-splitting
//   // this generates a separate chunk (about.[hash].js) for this route
//   // which is lazy-loaded when the route is visited.
//   component: () =>
//     import(/* webpackChunkName: "about" */ "../views/About.vue"),
// },
