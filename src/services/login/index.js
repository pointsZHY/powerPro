import apis from "@/config/api.js";
import requestFn from "@/utils/request";

//登陆
export function LoginFN(params) {
  return requestFn({
    url: apis.login.login,
    methods: "post",
    data: params,
  });
}
