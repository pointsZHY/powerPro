// const IP = "http://localhost:8801";
// const IP = "http://192.168.1.135:8801";
const PROTOCOL = 'http';
// // const SERVICEIP="39.100.207.199";
const SERVICEIP="39.100.207.199";
const PORT = '80';
const PREFIX = 'api'; // 测试环境
const IP = `${PROTOCOL}://${SERVICEIP}:${PORT}/${PREFIX}`;
// const IP = `/${PREFIX}`;

export { IP };
