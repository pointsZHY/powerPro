const apis = {
  common:{
    getOrgType:"/org/type/{type}",
    udc:"/udc/udc",
    udcList:"/udc/udcList"
  },
  login: {
    login: "/user/login",
  },
  cable:{
    add:"/cable/cable",
    list:"/cable/cablePage",
    del:"/cable/deleteCables",
    importExecl:"/excel/import/cable",
    saveBatch:'/cable/saveBatch'
  },
  devices:{
    add:"/devices/devices",
    list:"/devices/devicesPage",
    del:"/devices/devicesBatch",
    allList:"/devices/devicesAll",
    importDev:"/excel/import/device"
  },
  devicesName:{
    add:"/devicesName/devicesName", 
    list:"/devicesName/devicesNamePage",
    del:"/devicesName/devicesNameBatch",
    importDev:"/excel/import/deviceName"
  },
  runLines:{
    add:"/line/line",
    list:"/line/linePage",
    del:"/line/deleteLines",
    allList:"/line/lineAll"
  },
  substation:{
    add:"/sub/sub",
    del:"/sub/subs",
    list:"/sub/subPage",
    allList:"/sub/subAll"
  },
  user:{
    add:"/user/user",
    list:"/user/userPage",
    del:"/user/deleteUsers",
    resSetpassword:"/user/initPassword/{id}"
  },
  org:{
    getTree:"/org/tree",
    saveOrg:"/org/org",
    delOrg:"/org/org/{id}"
  },
  map:{
    getSub:"/sub/subAllByTeamCode/{teamCode}",//根据维护班组获取变电站
    getRunLines:"/line/lineAllByTeamCode/{teamCode}",//根据班组获取线路
    getLines:"/line/allDevicesByLine/{lineCode}",// 根据运行线路ID 获取全部设备和导线 电缆
    getDevByLineCodeAndDevType:"/line/allDevicesByLineAndDevType/{lineCode}/{devtype}",//根据运行线路id 和设备类型获取设备
    getNameByLineId:"/devicesName/findByParentId/{id}",//根据线路id 获取设备文字
  },
  conversion:{
    conversionPonts:"/devices/conversion",//坐标系转换
    conversionPontsCab:"/cable/conversion",//电缆电线
    conversionPontsSub:"/sub/conversion",//变电站
    conversionPontsDevName:"/devicesName/conversion",//配网设备名称
  }
};

export default apis;
