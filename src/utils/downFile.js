/**
 * 通过链接下载文件
 * @param {Object} link 下载链接
 * @param {Object} fileName 下载的文件名称
 */
const downloadWebFile = (link = "", fileName = "") => {
  console.log("....",link)
  console.log("....",fileName)

  if ('msSaveOrOpenBlob' in window.navigator) {
    // IE,Edge浏览器
    window.navigator.msSaveOrOpenBlob(link, fileName)
  } else {
    const a = document.createElement('a')
    a.style.display = 'none'
    document.body.appendChild(a)
    a.href = link
    a.download = fileName
    a.click()
    document.body.removeChild(a)
  }
}
export default downloadWebFile
