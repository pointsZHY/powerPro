import axios from "axios";
import { IP } from "@/config/host";
import { Loading,Message } from 'element-ui';
const requestFn = ({ url, methods, data }) => {
  let loadingInstance = Loading.service({ fullscreen: true });
  return new Promise((resolve, reject) => {
    let axiosConfig = {
      url: `${IP}${url}`,
      method: methods,
      headers: {
        Token: "12121212",
      },
    };
    switch (methods.toLowerCase()) {
      case "get":
        axiosConfig.params = data;
      case "post":
        axiosConfig.data = data;
      case "put":
        axiosConfig.data = data;
      case "delete":
        axiosConfig.data = data;
    }
    axios(axiosConfig)
      .then((res) => {
        loadingInstance.close()
        resolve(res);
      })
      .catch((err) => {
        loadingInstance.close()
        console.log(",,,",err.response)
        reject(err);
        if(err.response.status==500) {
          Message({
            type:"error",
            message:"系统错误,请联系管理员"
          })
        }
        console.log("err");
      });
  });
};

export default requestFn;
