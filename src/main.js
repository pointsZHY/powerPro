import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import requestFn from './utils/request';
import apis from './config/api';
console.log("apis",apis)
// import VueAMap from "vue-amap";
Vue.config.productionTip = false;
Vue.use(ElementUI);
// Vue.use(VueAMap);

// // 初始化vue-amap
// VueAMap.initAMapApiLoader({
//   // 高德的key
//   key: "YOUR_KEY",
//   // 插件集合
//   plugin: [
//     "AMap.Autocomplete",
//     "AMap.PlaceSearch",
//     "AMap.Scale",
//     "AMap.OverView",
//     "AMap.ToolBar",
//     "AMap.MapType",
//     "AMap.PolyEditor",
//     "AMap.CircleEditor",
//   ],
//   // 高德 sdk 版本，默认为 1.4.4
//   v: "1.4.4",
//   mapStyle: "amap://styles/whitesmoke",
// });

Vue.prototype.$request=requestFn
Vue.prototype.$api=apis


new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
