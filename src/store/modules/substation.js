// import router from "@/router";
const state = {
  searchParam:{
    current:1,
    size:10,
    conversion:"2"
  },
  subForm:{
    name:"",
    teamCode:"",
    longitude:"",
    latitude:"",
    runTime:"",
    remark:""
  },
};
const mutations = {
  updateForm(state,params){
    state.subForm=JSON.parse(JSON.stringify(params))
  },
  updateSearch(state,params){
    state.searchParam=JSON.parse(JSON.stringify(params))
  }
};
const actions = {};
export default {
  namespaced: true, //namespaced为false的时候，state,mutations,actions全局可以调用，为true，生成作用域，模块下可以调用
  state,
  mutations,
  actions,
};
