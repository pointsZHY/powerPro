import { LoginFN } from "@/services/login";
import router from "@/router";
import { Message } from 'element-ui';
const state = {
  userInfo: {},
};
const mutations = {};
const actions = {
  async userLogin({ commit }, params) {
    console.log("paramsparams",params)
    const res = await LoginFN(params);
    console.log("res", res);
    if(res.data.status==200){
      window.localStorage.setItem("userinfo",JSON.stringify(res.data.data))
      router.push("/home/map");
    }else{
      Message({
        message:res.data.msg,
        type:"error"
      })
    }
  },
};
export default {
  namespaced: true, //namespaced为false的时候，state,mutations,actions全局可以调用，为true，生成作用域，模块下可以调用
  state,
  mutations,
  actions,
};
