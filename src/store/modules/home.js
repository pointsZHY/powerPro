import { LoginFN } from "@/services/login";
import router from "@/router";
const state = {
  menus: [
    {
      name: "地图",
      index: "/home/map",
    },
    {
      name: "设备管理",
      index: "/home/dev",
      children: [
        {
          name: "运行线路",
          index: "/home/runLines",
        },
        {
          name: "变电站",
          index: "/home/substation",
        },
        {
          name: "配网设备",
          index: "/home/devices",
        },
        {
          name: "配网设备名称",
          index: "/home/devicesName",
        },
        {
          name: "导线-电缆",
          index: "/home/lines",
        },
      ],
    },
    {
      name: "系统管理",
      index: "/home/sys",
      children: [
        {
          name: "用户管理",
          index: "/home/user",
        },
        {
          name: "组织管理",
          index: "/home/org",
        },
        {
          name: "UDC管理",
          index: "/home/udc",
        },
      ],
    },
  ],
};
const mutations = {};
const actions = {};
export default {
  namespaced: true, //namespaced为false的时候，state,mutations,actions全局可以调用，为true，生成作用域，模块下可以调用
  state,
  mutations,
  actions,
};
