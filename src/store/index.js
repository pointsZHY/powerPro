import Vue from "vue";
import Vuex from "vuex";
import login from "./modules/login";
import home from "./modules/home";
import userManage from "./modules/userManage";
import devices from "./modules/devices";
import substation from "./modules/substation";
import lines from "./modules/lines";
import runLines from './modules/runLines';
import udcManage from './modules/udcManage';
import devNameForm from './modules/devNameForm';
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    login,
    userManage,
    home,
    devices,
    substation,
    lines,
    runLines,
    udcManage,
    devNameForm
  },
});
